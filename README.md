# Projeto CRUD em PHP - Bootstrap, PDO & POSTGRES SQL

Desenvolvimento de um simples projeto CRUD (agenda de contatos) utilizando o acesso a banco de dados com o Postgres SQL e linguagem PHP.

## Assuntos Abordados no Desenvolvimento do Projeto:

- Acesso a banco de dados com o Postgres SQL
- Otimização da conexão com o banco de dados através do PDO (PHP Data Object)
- Uso de linguagens, como: JavaScript e CSS
- Uso do framework Bootstrap para realização de um layout responsivo para o projeto.

## Configuração do Projeto:

- Executar a query pessoa.sql ou importar o arquivo no phpMyAdmin para criar a table necessária.
- Editar o arquivo **banco.php** 

```
$dbNome = 'nomeDaTable' 
$dbHost = 'nomeDoDominioOuIP:Porta' 
$dbUsuario = 'usuarioDoPostgres' 
$dbSenha 'senhaDoUsuario'

```
