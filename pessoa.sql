-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE public.pessoa
(
  id integer NOT NULL DEFAULT nextval('pessoa_id_seq'::regclass),
  nome character varying(50) NOT NULL,
  endereco character varying(80) NOT NULL,
  telefone character varying(35) NOT NULL,
  email character varying(40) NOT NULL,
  sexo character varying(1) NOT NULL,
  CONSTRAINT pessoa_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.pessoa
  OWNER TO postgres;
