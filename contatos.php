<?php
require 'banco.php';
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <title>Lista de Contatos</title>
</head>
<style>
    .jumbotron {
        padding: 0rem 2rem;
    }
</style>

<body>
<?php include("cabecalho.php"); ?>

<div class="container">
    <div class="jumbotron">
        <div class="row">
            <h2>Lista de Contatos<span class="badge badge-secondary"></span></h2>
        </div>
    </div>

    <!--Modal Adicionar-->
    <div class="float-right">
        <?php include("create-modal.php"); ?>
    </div>
    <br>
    <br>

    <div class="row">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Endereço</th>
                <th scope="col">Telefone</th>
                <th scope="col">E-mail</th>
                <th scope="col">Sexo</th>
                <th scope="col">Ação</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $pdo = Banco::conectar();
            $sql = 'SELECT * FROM pessoa ORDER BY id DESC';

            foreach ($pdo->query($sql) as $row) {
                $sexo = $row['sexo'] == 'M' ? 'Masculino' : 'Feminino';
                echo '<tr>';
                echo '<td>' . $row['nome'] . '</td>';
                echo '<td>' . $row['endereco'] . '</td>';
                echo '<td>' . $row['telefone'] . '</td>';
                echo '<td>' . $row['email'] . '</td>';
                echo '<td>' . $sexo . '</td>';
                echo '<td width=250>';
                echo '<a class="btn btn-primary" href="read.php?id=' . $row['id'] . '">Info</a>';
                echo ' ';
                echo '<a class="btn btn-warning" href="update.php?id=' . $row['id'] . '">Atualizar</a>';
                echo ' ';
                echo '<a class="btn btn-danger" href="delete.php?id=' . $row['id'] . '">Excluir</a>';
                echo '</td>';
                echo '</tr>';
            }
            Banco::desconectar();
            ?>
            </tbody>
        </table>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="assets/js/bootstrap.min.js"></script>
</body>

</html>
